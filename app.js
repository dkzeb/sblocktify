const network = require('./musiconomy-network');
const spotifyHandler = require('./spotifyHandler');
const cmdargs = require('command-line-args');
const cmddef = [
  { name: 'port', alias: 'p', type: Number },
  { name: 'shouldConnect', alias: 's', type: Boolean },
  { name: 'bsip', alias: 'b', type: String },
  { name: 'bsport', alias: 'i', type: String },
  { name: 'nogui', type: Boolean}
];

const args = cmdargs(cmddef);

if(args.nogui){
    network.createServer(args.port, () => { console.log("Add Song"); }, () => { console.log("Handle Points!") });
    network.connect(args.bsip, args.bsport, "id_"+args.port, args.port);
} else {
  spotifyHandler.createSpotifyHandler(args.port, network);
}
