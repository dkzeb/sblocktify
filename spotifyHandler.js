var express = require('express');
var app = module.exports = express();
var path = require('path');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var SpotifyWebApi = require('spotify-web-api-node');
var expressWS = require('express-ws')(app);
var cors = require('cors');

    app.httpPort;

var index = require('./src/spotifyHandler/routes/index');
var callback = require('./src/spotifyHandler/routes/oauthCallback');
var playlist = require('./src/spotifyHandler/routes/add');
var playlistSetup = require('./src/spotifyHandler/routes/playlistSetup');
var playlistInformation = require('./src/spotifyHandler/routes/playlistInformation');
var songs = require('./src/spotifyHandler/routes/songs');
var songVoting = require('./src/spotifyHandler/routes/songVoting');


var createSpotifyHandler = (port, network) => {

    app.network = network;
    app.httpPort = port;
    app.wsPort = (parseInt(port)+1000);

    app.handleSong = function({songId, requesterId}, shouldStartVote){

        if(!app.playlistID){
            return console.error("No playlist created yet or ID not set!");
        }
        console.log("Someone tried to add a song!", songId);
        app.spotifyApi.getMe().then((data) => {
            let spotifyUsername = data.body.id;
            app.spotifyApi.addTracksToPlaylist(spotifyUsername, app.playlistID, ["spotify:track:"+songId]).then((data) => {
                console.log("We added a song",songId, "Spotify Data", data);
            }).catch((err) => {
              console.log("Spotify AddTrack Error", err);
            });
        }).catch((err) => {
            console.error("Spotify GetMe Error", err);
        })
        if(shouldStartVote){
	        songs.tellClientSongWasAdded(songId, requesterId); //SONG ID ONLY OTHERWISE EVERYTHING WILL BLOW UP.
        }
    };
    
    app.handlePoints = function(points) {
	    if(!app.playlistID){
		    return console.error("No playlist created yet or ID not set!");
	    }
	    songs.tellClientPointsWereChanged(points);
    }


    var scopes = ['user-read-private', 'user-read-email', 'playlist-modify-public', 'user-follow-modify', 'user-read-playback-state', 'user-read-currently-playing', 'user-modify-playback-state'],
        redirectUri = 'http://localhost:' + app.httpPort + '/callback/', //TODO: PORT SHOULD NOT BE HARDCODED
        clientId = '065d9fc48ba2403883bb4d43e61c5324',
        clientSecret = '737d89af29354b4282f26f30fa9ec00b',
        state = 'some-state-of-my-choice'; //TODO: randomize and ckeck on callback

    app.spotifyApi = new SpotifyWebApi({
        scopes : scopes,
        redirectUri: redirectUri,
        clientId: clientId,
        clientSecret : clientSecret,
        state : state
    });

    app.authorizeURL = app.spotifyApi.createAuthorizeURL(scopes, state);


    // view engine setup
    app.set('views', path.join(__dirname, 'views'));
    app.set('view engine', 'hbs');

    // uncomment after placing your favicon in /public
    //app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(cookieParser());
    app.use(express.static(path.join(__dirname, 'public')));

    //Route handling
    app.use('/', index);
    app.use('/callback', callback);
    app.use('/playlist', playlist);
    app.use('/playlistSetup', playlistSetup);
    app.use('/playlistInformation', playlistInformation);
    app.use('/songs', songs.router);
    app.use('/songVoting', songVoting);


    // catch 404 and forward to error handler
    app.use(function(req, res, next) {
        var err = new Error('Not Found');
        err.status = 404;
        next(err);
    });

    // error handler
    app.use(function(err, req, res, next) {
        // set locals, only providing error in development
        res.locals.message = err.message;
        res.locals.error = req.app.get('env') === 'development' ? err : {};

        // render the error page
        res.status(err.status || 500);
        res.render('error');
    });

    app.listen(app.httpPort);

    console.log("Sblocktify running on port:", app.httpPort );

}

module.exports = {
    createSpotifyHandler
}
