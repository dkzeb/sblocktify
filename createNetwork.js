// spawn that shit
const { spawn } = require('child_process');
const count = process.argv[2];

var nobs = process.argv[3];

// events helper
var attachEvents = (obj, id) => {
  obj.stdout.on('data', (data) => {
    console.log(`${id}: ${data}`);
  });

  obj.stderr.on('data', (data) => {
    console.error(`${id}: ${data}`);
  });

  obj.on('close', (code) => {
    console.log(`child process exited with code ${code}`);
  });
}


console.log('Spawning network with', count, 'nodes!', nobs);

if(nobs !== "nobootstrap"){
  const bs = spawn('node', ['app.js', '--port', '3000']);
  attachEvents(bs, "BS");
}

for(var i = 1; i < count; i++){
  let port = 4000+i;
  let instance = spawn('node', ['app.js', '--shouldConnect', '--bsip', 'localhost', '--bsport', '4000', '--port', port, '--nogui']);
  attachEvents(instance, port);


  var waitTill = new Date(new Date().getTime() + 0.25 * 1000);
  while(waitTill > new Date()){}
}
