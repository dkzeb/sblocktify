var app = require('../../../spotifyHandler.js');
var express = require('express');
var router = express.Router();

  router.post('/:playlistName', function(req, res) {
    //router.get('/:playlistName', function(req, res) { //for in-browser testing
    var spotifyUsername;
    var playlistName = req.params.playlistName;

    //console.log("WSP", app.wsPort);

    //console.log("Creating network! at port:", app.wsPort);
    app.network.createNetwork(playlistName, app.wsPort);
    createSpotifyPlaylist(playlistName, res);
    app.network.createServer(app.wsPort, app.handleSong, app.handlePoints);
});

router.post('/join/:playlistName', function(req, res){
    var playlistName = req.params.playlistName;
    var bsip = req.body.bshost;
    var bsport = req.body.bsport;
    createSpotifyPlaylist(playlistName, res, () => {
      app.network.createServer(app.wsPort, app.handleSong, app.handlePoints);
      app.network.connect(bsip, bsport, "id_"+app.wsPort, app.wsPort);
    });
});

router.put('/:playlistID/:songID', function(req, res) {  //todo: SPØRG MUSICONOMY NETVORKET OM AT TILFØJE DEN HER SANG
    var spotifyUsername;
    var playlistID = req.params.playlistID;
    var songID = req.params.songID; // req.body.songs;

    //songIDs = JSON.parse(songIDs);

    console.log("\n\n------addStuff------\n\n");
    console.log("Trying to add song(s) to a playlist: ");
    console.log("PlaylistID:" , playlistID);
    console.log("SongID:" , songID);
    var parsedSongID = songID.replace("spotify:track:", "");
    console.log("ParsedID", parsedSongID);
    app.network.addSongRequest(parsedSongID);
    res.status(201).send(parsedSongID+" sent to network for verification!");

    });

var createSpotifyPlaylist = function(playlistName, res, cb = null) {
  app.spotifyApi.getMe()
      .then(function (data) {
          spotifyUsername = data.body.id;
          console.log("Trying to create playlist:");
          console.log("spotify username: " , spotifyUsername);
          console.log("Playlistname: ", playlistName);
      }, function (err) {
          console.log('add.js - POST - Could not get user data.. Something went wrong!', err);
          res.status(500);
          res.send('No deal');
      }).then(function (data) {
      app.spotifyApi.createPlaylist(spotifyUsername, playlistName, {'public': true})
          .then(function (data) {
              console.log('Created playlist!');
              app.playlistID = data.body.uri.split(':')[4];
              if(cb) {
                cb();
              } // run the callback
              res.status(201).send({"userID":spotifyUsername,"playlistName":playlistName,"playlistID":data.body.uri.split(':')[4]});
              //res.send('playlist created! ID: ' + data.body.uri.split(':')[4] + ' ');  //data comes back looking like: spotify:user:markrobotto:playlist:5oa7xJjtO9phBpvBNPA0Sl
          }, function (err) {
              res.status(500);
              res.send('Pokemon error handling in add.js, POST says: Something went wrong: '+err);
              console.log('Pokemon error handling in add.js, POST says: Something went wrong: '+err);
          })
  });
}

module.exports = router;
