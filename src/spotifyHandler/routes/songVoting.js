var express = require('express');
var router = express.Router();
var app = require('../../../spotifyHandler.js');

var vote;
var requesterID;

router.put('/:requesterID/:vote', function(req,res){

    requesterID = req.params.requesterID;
    vote = req.params.vote;
    
    app.network.makeVote(requesterID, vote);

    console.log("Voted " + vote + " for " + requesterID);

    res.send(200);

});


module.exports = router;