var express = require('express');
var router = express.Router();
var app = require('../../../spotifyHandler.js');

/* GET home page. */
router.get('/', function (req, res, next) {


// Create the authorization URL
    console.log('\n ---- \n autorizeURL:\n' + app.authorizeURL + '\n ---- \n');

    res.redirect(app.authorizeURL);

    //res.render('index', {title: 'Express'});
});

module.exports = router;
