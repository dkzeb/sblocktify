var express = require('express');
var router = express.Router();
var app = require('../../../spotifyHandler.js');

/* GET home page. */

router.get('/', function(req, res) {

        var code = req.query.code;

        app.spotifyApi.authorizationCodeGrant(code)
        .then(function(data) {
            console.log('The token expires in ' + data.body['expires_in']);
            console.log('The access token is ' + data.body['access_token']);
            console.log('The refresh token is ' + data.body['refresh_token']);

            // Set the access token on the API object to use it in later calls
            app.spotifyApi.setAccessToken(data.body['access_token']);
            app.spotifyApi.setRefreshToken(data.body['refresh_token']);

            app.spotifyApi.getMe()
                .then(function (data) {
                    var spotifyUsername = data.body.id;

                    res.status(200);
                    res.redirect('../playlistSetup');

                    console.log(spotifyUsername,"sucessfully logged in!");
                }, function (err) {
                    console.log('oauthCallback.js - Could not get user data.. Something went wrong!', err);
                    res.status(500);
                    res.send('oauthcallback.js - failed to get user data');
                })

        }, function(err) {
            console.log('authorizationCodeGrant in oauthCallback.js - Something went wrong!', err);
            res.status(500);
            res.send('Pokemon error handling in oauthCallback.js says:\nSomething went wrong: '+err);
        });

        setInterval(() => refreshAccessToken(), 60000*30);

});

function refreshAccessToken() {
    app.spotifyApi.refreshAccessToken()
        .then(function(data) {
            console.log('The access token has been refreshed!');

            // Save the access token so that it's used in future calls
            app.spotifyApi.setAccessToken(data.body['access_token']);
        }, function(err) {
            console.log('Could not refresh access token', err);
        });
}

module.exports = router;

