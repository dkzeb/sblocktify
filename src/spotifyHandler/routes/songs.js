module.exports = router;
var app = require('../../../spotifyHandler.js');
var express = require('express');
var expressWs = require('express-ws')(express);
var router = express.Router();
var ip = require('ip');

var connectedSocket;

router.ws('/', (ws, req) => {
    connectedSocket=ws;
    console.log("WS: Client lives :O");

    ws.on('close', function () {
       console.log("WS: Client died :( ");
    });

    ws.on('message', function(msg) {
        console.log("WS: Client spoke:", msg);
    });

});

/* GET home page. */
router.get('/', function(req, res) {
    var spotifyUsername;

    app.spotifyApi.getMe()
        .then(function (data) {
            spotifyUsername = data.body.id;
            res.render('songs', {
                title: 'Add songs to the shared playlist',
                username: spotifyUsername,
                accessToken: app.spotifyApi.getAccessToken(),
                playlistID: app.playlistInformation.playlistID,
                playlistName: app.playlistInformation.playlistName,
                userID: app.playlistInformation.userID,
                serverIP: ip.address(),
                serverPort: app.httpPort
            });
            console.log("PlaylistID: ", app.playlistInformation.playlistID, "USerID:", app.playlistInformation.userID);
            console.log("spotify username: " , spotifyUsername);
            console.log("accesstoken " , app.spotifyApi.getAccessToken())
        }, function (err) {
            console.log('Could not get user data.. Something went wrong!', err);
        })

});

var tellClientSongWasAdded = function(songId, requesterId){
  if(connectedSocket){
	  connectedSocket.send(JSON.stringify({type: "song", payload: { songId, requesterId }}));
	  console.log("Someone tried to add a song!", songId);
  } else {
      console.log("tellClientSongWasAdded: not connected to client");
  }
};

var tellClientPointsWereChanged = function(newPointSum) {
    if(connectedSocket) {
        connectedSocket.send(JSON.stringify({type: "points", payload: newPointSum}));
        console.log("Points were updated:", newPointSum);
    } else {
	    console.log("tellClientPointsWereChanged: not connected to clint");
    }
}

module.exports = {
    router,
    tellClientSongWasAdded,
    tellClientPointsWereChanged
};
