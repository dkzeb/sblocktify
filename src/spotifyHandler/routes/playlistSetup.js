var express = require('express');
var router = express.Router();
var app = require('../../../spotifyHandler.js');

/* GET home page. */
router.get('/', function (req, res) {
    res.render('playlistSetup', {
    	httpPort: app.httpPort
	});
});
module.exports = router;