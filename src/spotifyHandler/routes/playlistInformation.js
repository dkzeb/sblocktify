var express = require('express');
var router = express.Router();
var app = require('../../../spotifyHandler.js');

/* GET home page. */
router.get('/', function (req, res, next) {
	console.log("You have requested playlist info!");
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	res.status(200).send(app.playlistInformation);
	console.log(app.localPlaylist);
});


router.post('/:playlistId/:playlistName/:userId', function (req, res) {
	var playlistId = req.params.playlistId;
	var playlistName = req.params.playlistName;
	var userId = req.params.userId;

	app.playlistInformation = {
		playlistID: playlistId,
		playlistName: playlistName,
		userID: userId
	};

	console.log("Playlist Info: ",app.playlistInformation);
	res.sendStatus(200);
});



module.exports = router;
