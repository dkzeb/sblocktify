const Block = require('./block');
const CryptoJS = require('cryptojs').Crypto;

/*var calculateHash = (index, prevHash, timestamp, data) => {
  return CryptoJS.SHA256(index + prevHash + timestamp + data).toString();
}*/
var calculateHash = (index, prevHash, data) => {
  let calcData;
  try {
    calcData = JSON.stringify(data);
  } catch(err) {
    calcData = data;
  }
  return CryptoJS.SHA256(index + prevHash + data).toString();
}

var hashObj = (objToHash) => {
  return CryptoJS.SHA256(objToHash).toString();
}

var calculateHashForBlock = (block) => {
    return calculateHash(block.index, block.prevHash, block.data);
};
var createGenesisBlock = (playlistName, id) => {
	// create new scoreboard
	// create new playlist with a name
	let genesisBlock = generateBlock({
		type: "genesis",
        rules: {
		    songCost: 3
        },
		scoreboard: {
		    [id]: 0
        },
		playlist: {
		    playlistName,
            tracks: []
        }
	});
	genesisBlock.index = 0;
	return genesisBlock;
}
var isValidNewBlock = (newBlock, previousBlock) => {
    if(!previousBlock && newBlock.data.type == "genesis")
        return true;

    if(!previousBlock){
      console.log("No prev block... wtf", blockchain);
      return false;
    }

    if (previousBlock.index + 1 !== newBlock.index) {
        console.log('Chain is invalid - invalid index');
        return false;
    } else if (previousBlock.hash !== newBlock.prevHash) {
        console.log('invalid previoushash');
        return false;
    } else if (calculateHash(newBlock.index, newBlock.prevHash, newBlock.data) !== newBlock.hash) {
        console.log('invalid hash: ' + calculateHash(newBlock.index, newBlock.prevHash, newBlock.data) + ' ' + newBlock.hash);
        return false;
    }
    return true;
};
var generateBlock = (blockData) => {
  //console.log("Generating new block with data", blockData);
  var prevBlock = getLatestBlock();
  if(!prevBlock){
      prevBlock = {
          index: 0,
          hash: ""
      }
  }
  var nextIndex = prevBlock.index + 1;
  var nextTimestamp = new Date().getTime() / 1000;
  var nextHash = calculateHash(nextIndex, prevBlock.hash, blockData);
  return new Block(nextIndex, prevBlock.hash, nextTimestamp, blockData, nextHash);
}
var addBlock = (newBlock, cb = null) => {
  if(isValidNewBlock(newBlock, getLatestBlock())){
    blockchain.push(newBlock);
  }
}

var getLatestBlock = () => blockchain[blockchain.length - 1];
var replaceChain = (newBlocks) => {

  // don't replace chain if its equal to the one we have
  if(hashObj(newBlocks) === hashObj(blockchain)){
    console.log("Chains are equal, do not replace!")
    return false;
  }

  if(isValidChain(newBlocks) && newBlocks.length > blockchain.length){
    console.log("Replacing chain with new blocks!");
    blockchain = newBlocks;
    return true;
  } else {
    return false;
  }
}
var isValidChain = (blockchainToValidate) => {
    /*if (JSON.stringify(blockchainToValidate[0]) !== JSON.stringify(createGenesisBlock())) {
        return false;
    }*/


    var tempBlocks = [blockchainToValidate[0]];
    for (var i = 1; i < blockchainToValidate.length; i++) {
        if (isValidNewBlock(blockchainToValidate[i], tempBlocks[i - 1])) {
            tempBlocks.push(blockchainToValidate[i]);
        } else {
            return false;
        }
    }
    return true;
};

var blockchain = [];

module.exports = {
  calculateHash,
  calculateHashForBlock,
  createGenesisBlock,
  isValidNewBlock,
  generateBlock,
  addBlock,
  getLatestBlock,
  replaceChain,
  isValidChain,
  getBlockchain: () => {
    return blockchain;
  }
}
