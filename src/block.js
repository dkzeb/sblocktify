class Block {
  constructor(index, prevHash, timestamp, data, hash){
    this.index = index;
    this.prevHash = prevHash.toString();
    this.timestamp = timestamp;
    this.data = data;
    this.hash = hash.toString();
    this.status = "prepare";
  }
  updateStatus(newstatus){
    this.status = newstatus;
   }
   
}

module.exports = Block;
