const WebSocket = require('ws');
const querystring = require('querystring');
const ip = require('ip');
require('events').EventEmitter.defaultMaxListeners = Infinity;
// peer-connection list to maintain seperate from the wss
var connectedPeers = {};

// the webserver created to host the websocket
const server = require('http').createServer();

// websocket imports
const wss = new WebSocket.Server({ server });

// block imports and blockchain array
const Block = require('./src/block');
const Utils = require('./src/block.utils.js');

/////// FIELD VARS
var thisPort = 0;
var playlist;
var scoreboard;
var rules;
var fRVotingTable = {};
var sRVotingTable = {};
var lastMsg = null;
var clientSongCallback;
var clientPointCallback = (p) => console.log("clientPontCallcack called before instantiated", p);

////// METHODS
var broadcast = (cmd, data = null) => {
	console.log("Broadcasting", cmd, "to the network!");
	Object.keys(connectedPeers).forEach((peer) => {

		// conn error handling
		if (connectedPeers[peer].connection){
			connectedPeers[peer].connection.on('error', (err) => {
				console.error(err);
			});

			if (data){
				cmd = cmd + ">" + data;
			}

			if (connectedPeers[peer].connection.readyState == 1){
				connectedPeers[peer].connection.send(cmd + ">" + data, (err) => {
					if (err){
						console.log("Sent", err);
					}
				});
			}

		} else {
			if (data){
				cmd = cmd + ">" + data;
			}
			if (connectedPeers[peer].instance){
				if (connectedPeers[peer].instance.readyState == 1){
					connectedPeers[peer].instance.send(cmd);
				}
			}
		}
	});
}
var connect = (remoteaddr, remoteport, id, myport) => {
	if (!remoteaddr || !remoteport){
		return console.error("Connection info is missing! \nIP", remoteaddr, "port", remoteport);
	}
	console.log("Connecting to", remoteaddr, remoteport);

	var socket = new WebSocket("ws://" + remoteaddr + ":" + remoteport + "/?id=" + id + "&addr=" + ip.address() + "&port=" + myport);
	socket.on('open', () => {
		// when the socket connects, ask for the chain and the connected peers of the network
		socket.send("getChain");
		socket.send("getPeers");
	});
	socket.on('message', (data) => {
		messageHandler(data, socket);
	});

	socket.on('error', (err) => {
		console.error(err);
	});

	return socket;
}
// the message handler is where we deal with all commands sent out over the network
// a command is built up as "command">"payload", or just "command" if no payload is present
var messageHandler = (data, ws) => {
	var data = data.split(">");
	if (data[1]){
		var payload = data[1];
	}
	switch (data[0]){
		// retrieve the peers currently connected to the node
		case "getPeers":
			// remove references to the ws-socket and server of the peer,
			// e.g. only send id, and ip information
			var peersToSend = {};

			Object.keys(connectedPeers).forEach((peer) => {
				var p = {
					ip: connectedPeers[peer].ip,
					port: connectedPeers[peer].port
				};
				peersToSend[peer] = p;
			});
			ws.send("updatePeers>" + JSON.stringify(peersToSend));
			break;
		// get the blockchain from the connected node (returns an update command)
		case "getChain":
			ws.send("updateChain>" + JSON.stringify(Utils.getBlockchain()));
			break;
		case "disconnectPeer":
			delete connectedPeers[payload];
			ws.send("removedPeer " + payload);
			break;
		case "updateChain":
			console.log("Updating the chain!");
			if (payload){
				payload = JSON.parse(payload);
			} else {
				payload = Utils.getBlockchain();
			}
			var replaced = Utils.replaceChain(payload);
			if (replaced){
				Utils.getBlockchain().forEach((block) => {
					//console.log("UpdateChainImplementBlock", block);
					implementBlock(block, false);
				});
			} else {
				console.log("No reason to replace or re-implement");
			}
			break;
		case "updatePeers":
			console.log("Updating/Replacing connected peers!");
			if (payload){
				payload = JSON.parse(payload);
			}
			// run through the keys of our network, if a new id shows up,
			// add it to the connected peer and initiate connections
			Object.keys(payload).forEach((peer) => {
				//console.log("Got Peer", peer);
				if (!(peer in connectedPeers) && peer !== "id_" + thisPort){
					console.log("We should add this peer!", peer);
					var p = payload[peer];
					connectedPeers[peer] = {
						instance: null, // how the fuck will i get this? will i even need it?
						connection: connect(p.ip, p.port, "id_" + thisPort, thisPort),
						ip: p.ip,
						port: p.port
					};
				}
			});
			break;
		case "refreshPeers":
			ws.send("getPeers");
			break;
		case "createBlock":
			createBlock(payload);
			break;
		case "preparedBlock":
			//console.log("Received Prepared Block from", ws.networkID, payload);
			vote(payload, ws.networkID, fRVotingTable);
			break;
		case "commitBlock":
			//console.log("Received Prepared Block from", ws.networkID, payload);
			vote(payload, ws.networkID, sRVotingTable);
			break;
		case "vote":
			var parsedVote = JSON.parse(payload);
			notateVote(parsedVote.requesterId, parseInt(parsedVote.vote));
			break;
		case "songAddRequest":
			try {
				payload = JSON.parse(payload);
			} catch (err){
				console.error(err);
				console.log(payload);
			}
			if (scoreboard[ws.networkID] && scoreboard[ws.networkID] >= 3){
				prepareData.scoreboard[ws.networkID] = -rules.songCost;
				prepareData.songRequests.push(payload);
			}
			break;
		// visualizer specific RCP
		case "getPLandSB":
			ws.send("sb>"+JSON.stringify(scoreboard)+">pl>"+JSON.stringify(playlist));
			break;
	}
}
var vote = (hash, id, votingTable) => {
	if (!votingTable[hash]){
		votingTable[hash] = {
			votes: 0,
			voters: []
		};
	}
	if (votingTable[hash].voters.indexOf(id) === -1){
		votingTable[hash].voters.push(id);
		votingTable[hash].votes += 1;
	}
}
var createBlock = (payload) => {
	try {
		payload = JSON.parse(payload);
	} catch (err){
		console.log("Not JSON adding as string");
	} finally {
		console.log("Create new block", payload);
		Utils.addBlock(Utils.generateBlock(payload));
		// broadcast to the network that a new block was generated
		broadcast("updateChain", JSON.stringify(Utils.getBlockchain()));
	}
}

var updateInterval;

var implementBlock = (block, activeBlock = true) => {
	if(!block) return console.error("No block...");
	
	let data = block.data;
	let type = data.type;
	switch (type){
		case "genesis":
			//console.log("Implementing Genesis block");
			//playlist = data.playlist.copyTo(playlist);
			//scoreboard = data.scoreboard;
			//rules = data.rules;
			try {
				playlist = {};
				scoreboard = {};
				rules = {};

				playlist = {
					playlistName: data.playlist.playlistName,
					tracks: data.playlist.tracks
				};
				Object.keys(data.scoreboard).forEach(function(key) {
					scoreboard[key] = data.scoreboard[key];
				});
				Object.keys(data.rules).forEach(function(key) {
					rules[key] = data.rules[key];
				});
				console.log(Date.now() + ":", "Implemented Genesis Block", playlist, scoreboard, rules);
			} catch (err){
				console.error("Implement Error", err);
			}
			break;
		case "action":
			data.backers.forEach((backer) => {
				if (!scoreboard[backer]){
					scoreboard[backer] = 0;
				}
				scoreboard[backer] += 1;
			});
			Object.keys(data.votes).forEach((key) => {
				if (!scoreboard[key]){ //Should exist but...
					scoreboard[key] = 0;
				}
				scoreboard[key] += data.votes[key];
			});
			data.songRequests.forEach((sr) => {
				playlist.tracks.push(sr.songId);
				clientSongCallback(sr, activeBlock);
			});
			Object.keys(data.scoreboard).forEach((key) => {
				scoreboard[key] += data.scoreboard[key];
			})
			console.log("id_" + thisPort + " Playlist:", playlist, "Scoreboard:", scoreboard);
			break
		default:
			console.log("The block did not match any of the types");
			break;
	}
	if (scoreboard["id_" + thisPort]){ //If we exist on the scoreboard we tell client our score
		clientPointCallback(scoreboard["id_" + thisPort])
	}
}
var createNetwork = (playlistName, myPort) => {
	// create genesisBlock
	let genesisBlock = Utils.createGenesisBlock(playlistName, "id_" + myPort);
	Utils.addBlock(genesisBlock);
	implementBlock(Utils.getLatestBlock());
}
var createServer = (port, songCallback, pointCallBack) => {
	thisPort = port;
	console.log("Creating Blockchain Network");

	wss.on('connection', function(ws, req) {
		console.log("Connection initiated!");
		// identify a peer from their 'hopefully' unique id
		var clientData = querystring.parse(req.url.split('/?')[1]);
		ws.networkID = clientData.id;
		// bind a function to the close event that also deletes the client from our connectedPeers
		ws.on('close', () => {
			console.log("Disconnecting peer", clientData.id);
			broadcast("disconnectPeer", clientData.id);
			delete connectedPeers[clientData.id];
		});

		ws.on('message', (data) => {
			messageHandler(data, ws);
		});


		// if there is no id or the id is already connected?
		if (!clientData.id){
			return;
		}

		if (connectedPeers[clientData.id]){
			// update the ws instance and then return
			connectedPeers[clientData.id].instance = ws;
			return;
		}

		connectedPeers[clientData.id] = {
			instance: ws,
			connection: connect(clientData.addr, clientData.port, "id_" + port, port),
			ip: clientData.addr,
			port: clientData.port
		}
		broadcast("refreshPeers");
	});
	updateInterval = setInterval(() => { //Todo: Can we just call in the known interval?
		let d = new Date();
		if (d.getSeconds() % 15 === 0 || d.getSeconds() === 0){
			var tmpBlock = Utils.generateBlock(prepareData);
			vote(tmpBlock.hash, "id_" + thisPort, fRVotingTable);
			console.log("id_" + thisPort, "Votingtable before timeout:", fRVotingTable);
			broadcast("preparedBlock", tmpBlock.hash);
			setTimeout(() => {
				let winningHash = null;
				let mostVotes = 0;
				Object.keys(fRVotingTable).forEach((hash) => {
					let vote = fRVotingTable[hash];
					if (vote.votes > mostVotes){
						winningHash = hash;
						mostVotes = vote.votes;
					}
				});
				console.log("id_" + thisPort, "Votingtable in timeout:", fRVotingTable);
				if (tmpBlock.hash === winningHash){
					console.log("id_" + thisPort, "We won first round! :) implement");
					tmpBlock.status = "comitted"; // We are ready for second round!
					prepareData.backers = fRVotingTable[winningHash].voters;
					let commitBlock = Utils.generateBlock(prepareData);  //Generate commit block with new hash
					commitBlock.status = "commit";
					vote(commitBlock.hash, "id_" + thisPort, sRVotingTable);
					broadcast("commitBlock", commitBlock.hash);
					setTimeout(() => {
						let winningHash = null;
						let mostVotes = 0;
						Object.keys(sRVotingTable).forEach((hash) => {
							let vote = sRVotingTable[hash];
							if (vote.votes > mostVotes){
								winningHash = hash;
								mostVotes = vote.votes;
							}
						});
						if (commitBlock.hash === winningHash){
							console.log("id_" + thisPort, "We won second round! :) implement");
							Utils.addBlock(commitBlock);
							implementBlock(Utils.getLatestBlock());
						} else {
							let winner = sRVotingTable[winningHash].voters[0];
							console.log("id_" + thisPort, "We lost second round! :/ - getting current chain from a winner!");
							setTimeout(() => {
								replaceChain(winner) }, 1000);
						}
						sRVotingTable = {};
						prepareData = prepDataObj();
					}, 2000);
				} else {
					//console.log("id_" + thisPort, "We lost first round! :/ ", tmpBlock.hash.substr(0, 5), winningHash.substr(0, 5));
					// The node is not allowed to vote in second round
					// wait a second to make sure the peer has updated his chain
					setTimeout(() => {
						var randomPeerIndex = getRandomInt(0, Object.keys(connectedPeers).length - 1);
						var randomPeer = connectedPeers[Object.keys(connectedPeers)[randomPeerIndex]];
						if (randomPeer){
							randomPeer.connection.send("getChain");
						}
					}, 3000)
				}
				// reset votes
				fRVotingTable = {};
				// reset our preparedData
			}, 2000);
		}
	}, 1000);

	var replaceChain = (aWinnerNode) => {
		try {
			console.log("Getting hash from", aWinnerNode);
			aWinnerNode.connection.send("getChain");
		} catch (err){
			console.log("Something went horribly wrong, ask EVERYBODY for the new chain");
			broadcast("getChain");
		}
	}

	server.listen(port, () => {
		console.log("Node is up", server.address().port);
	});

	clientSongCallback = songCallback;
	clientPointCallback = pointCallBack;
}
var addSongRequest = (songId) => {
	console.log("Adding Song Request", songId);
	// the node wants to add a song, and has not ask the network if its okay
	// put it into the preparedBlock statement for next round
	prepareData.scoreboard["id_" + thisPort] = -rules.songCost;
	let songRequest = { requesterId: "id_" + thisPort, songId: songId }
	prepareData.songRequests.push(songRequest);
	broadcast("songAddRequest>" + JSON.stringify(songRequest));
}
var prepDataObj = () => {
	return {
		type: "action",
		songRequests: [],
		scoreboard: {},
		votes: {}
	};
}
var prepareData = prepDataObj();

var getRandomInt = (min, max) => {
	min = Math.ceil(min);
	max = Math.floor(max);
	return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}
var makeVote = (requesterId, vote) => {
	notateVote(requesterId, vote);
	broadcast("vote", JSON.stringify({ requesterId, vote }));
}

var notateVote = (requesterId, vote) => {
	if (!prepareData.votes[requesterId]){
		prepareData.votes[requesterId] = 0;
	}
	prepareData.votes[requesterId] += parseInt(vote);

}
module.exports = {
	createServer,
	createNetwork,
	connect,
	createBlock,
	addSongRequest,
	makeVote
}
